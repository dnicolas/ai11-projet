# Description
Ontologie dans le cadre de l'UV AI11. Ontologie liée à une veille technologique portant sur les méthodes d'authentification aux systèmes informatiques. L'ontologie est basée sur l'ontologie STAC (Security Toolbox : Attacks and Countermeasures) disponible sur [DBpedia](https://databus.dbpedia.org/ontologies/securitytoolbox.appspot.com/stac).

# Contributeurs
Yassine Ouraq - Dimitri Nicolas - GI03 Université de Technologie de Compiègne